import json
from unittest import TestCase
import endpoint as Gql

hasura = Gql.Endpoint(url="http://206.189.44.166:8080/v1alpha1/graphql", secret="FGQYs5k58zBjUNUh")


# noinspection SpellCheckingInspection
class TestEndpoint(TestCase):

    def test_read(self):
        weather_station = "410C93"
        query = "{messages(where: {messages_device: {code: {_eq: \"" \
                + weather_station + "\"}}}, order_by: {created_at: asc} ) {payload}}"

        j = hasura.execute_query(query)
        messages = j["data"]["messages"]

        assert len(messages) > 0

